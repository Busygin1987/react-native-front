import React from 'react';
import { View } from 'react-native';

import SetupFont from "./src/utils/setup/fontSetup";
import Navigator from '.src/navigation';

export default function App() {
  return (
    <View>
      <SetupFont />
      <Navigator />
    </View>
  );
}
