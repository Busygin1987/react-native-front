import React from 'react'
import { StyleSheet } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Left, Body, Right, Button, Icon, Title } from 'native-base';

const Login = () => {
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')

  return (
    <Container>
      <Header>
      <Left>
        <Button transparent>
          <Icon name='arrow-back' />
        </Button>
      </Left>
      <Body>
        <Title>Login</Title>
      </Body>
      <Right>
        <Button transparent>
          <Icon name='menu' />
        </Button>
      </Right>
      </Header>
      <Content>
        <Form>
          <Item fixedLabel>
            <Label>Email</Label>
            <Input
              onChangeText={text => setEmail(text)}
              autoCompleteType={email}
              placeholder={'Enter your email'}
              value={email}
            />
          </Item>
          <Item fixedLabel last>
            <Label>Password</Label>
            <Input
              onChangeText={text => setPassword(text)}
              autoCompleteType={password}
              placeholder={'Enter your password'}
              secureTextEntry={true}
              value={password}
            />
          </Item>
        </Form>
      </Content>
    </Container>
  );
}

export default Login;

// const styles = StyleSheet.create({
//   container: {
//     flex: 1
//   }
// })
