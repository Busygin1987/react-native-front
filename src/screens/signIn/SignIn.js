import React from 'react';
import { StyleSheet } from 'react-native'

const SignIn = () => {
  const [firstName, setFirstName] = React.useState('')
  const [lastName, setLastName] = React.useState('')
  const [email, setEmail] = React.useState('')
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [confirmPassword, setConformPassword] = React.useState('')

  return (
    <Container>
      <Header>
      <Left>
        <Button transparent>
          <Icon name='arrow-back' />
        </Button>
      </Left>
      <Body>
        <Title>Sign In</Title>
      </Body>
      <Right>
        <Button transparent>
          <Icon name='menu' />
        </Button>
      </Right>
      </Header>
      <Content>
        <Form>
          <Item fixedLabel>
            <Label>First name</Label>
            <Input
              onChangeText={text => setFirstName(text)}
              autoCompleteType={email}
              placeholder={'Enter your first name'}
              value={firstName}
            />
          </Item>
          <Item fixedLabel>
            <Label>Last name</Label>
            <Input
              onChangeText={text => setLastName(text)}
              autoCompleteType={email}
              placeholder={'Enter your last name'}
              value={lastName}
            />
          </Item>
          <Item fixedLabel>
            <Label>Email</Label>
            <Input
              onChangeText={text => setEmail(text)}
              autoCompleteType={email}
              placeholder={'Enter your email'}
              value={email}
            />
          </Item>
          <Item fixedLabel>
            <Label>Password</Label>
            <Input
              onChangeText={text => setPassword(text)}
              autoCompleteType={password}
              placeholder={'Enter your password'}
              secureTextEntry={true}
              value={password}
            />
          </Item>
          <Item fixedLabel last>
            <Label>Confirm your password</Label>
            <Input
              onChangeText={text => setConformPassword(text)}
              autoCompleteType={password}
              placeholder={'Enter your password again'}
              secureTextEntry={true}
              value={confirmPassword}
            />
          </Item>
        </Form>
      </Content>
    </Container>
  );
}

export default SignIn;

// const styles = StyleSheet.create({
//   container: {
//     flex: 1
//   }
// })