import React from 'react';
import { StyleSheet, View, Text } from 'react-native'

const Main = () => {
  return (
    <View style={styles.container}>
      <Text>Main</Text>
    </View>
  )
}

export default Main;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
