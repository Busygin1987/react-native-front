import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import Login from './screens/login/Login';
import SignIn from './screens/signIn/SignIn';
import Main from './screens/main/Main';

const Stack = createStackNavigator({
  Login: {
      screen: Login,
      navigationOptions: {}
  },
  SignIn: {
      screen: SignIn,
      navigationOptions: {}
  },
  Main: {
    screen: Main,
    navigationOptions: {}
  },
},
{
  initialRouteName: 'Main'
});

export default createAppContainer(Stack);